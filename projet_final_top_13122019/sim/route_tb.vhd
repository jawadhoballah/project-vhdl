library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;

library std_developerskit;
use std_developerskit.std_iopak.all;
use std.textio.all;

entity route_tb is
	generic (	clk_period_gen		: integer := 10	-- clock period
	);
end route_tb;


-------------------------------------------------------------------------------
-- tb1 - route avec reset async. 
-------------------------------------------------------------------------------

architecture tb1 of route_tb is
	constant	CLK_PERIOD 		: time	:= (clk_period_gen) * 1 ns;

	signal clk 					: std_logic := '0';
	signal reset_high 			: std_logic := '0';
	signal start				: std_logic := '1';
	signal route_l4 			: std_logic_vector( 15 downto 0) := (others=>'0');
	signal route_l3 			: std_logic_vector( 15 downto 0) := (others=>'0');
	signal route_l2 			: std_logic_vector( 15 downto 0) := (others=>'0');
	signal route_l1 			: std_logic_vector( 15 downto 0) := (others=>'0');
begin
	DUT: entity work.route
	port map (
		clk 				=> clk,
		reset 				=> reset_high,
		collision 			=> '0',
		start				=> '1',
		matrice_route_l4 	=> route_l4,
		matrice_route_l3 	=> route_l3,
		matrice_route_l2 	=> route_l2,
		matrice_route_l1 	=> route_l1
	);

	clk <= not clk after CLK_PERIOD/2;

	-- First reset all components
	reset_high 	<= '1' after 2 ns, '0' after 4 ns, '1' after 100 ns, '0' after 106 ns;

end tb1;