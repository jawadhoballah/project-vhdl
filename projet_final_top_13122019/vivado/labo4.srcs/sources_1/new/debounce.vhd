-- Debounce d'un bouton implicite

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity debounce is
	generic (
		db_clock_repeat	: integer := 22			-- 2^db_clock_repeat 
	);
	port(
		clk, reset		: in	std_logic;
		sw				: in	std_logic;
		db_level		: out 	std_logic
	);
end debounce;

architecture fsmd_arch of debounce is
	type state_type is (zero, one_wait, one, zero_wait);
	signal state_reg, state_next	: state_type;
	signal q_reg, q_next			: unsigned( db_clock_repeat-1 downto 0);
begin
	-- FSMD state & data registers
	process( clk, reset )
	begin
		if reset = '1' then
			state_reg <= zero;
			q_reg <= (others => '0' );
		elsif rising_edge(clk) then
			state_reg <= state_next;
			q_reg <= q_next;
		end if;
	end process;

	-- next-state logic & data path functional units
	process( state_reg, q_reg, sw, q_next )
	begin
		state_next 	<= state_reg;
		q_next		<= q_reg;
		case state_reg is
			when zero =>		-- btn has been released and doesn't bounce anymore
				db_level	<= '0';
				if sw = '1' then
					state_next	<= one_wait;
					q_next		<= (others => '1');
				end if;
			when one_wait =>	-- btn has been pushed and bounce
				db_level	<= '1';
				q_next		<= q_reg - 1;
				if q_next = 0 then
					state_next  <= one;
				end if;
			when one =>			-- btn has been pushed and doesn't bounce anymore
				db_level	<= '1';
				if sw = '0' then
					state_next	<= zero_wait;
					q_next		<= (others => '1');
				end if;
			when zero_wait =>	-- btn has been released and bounce
				db_level	<= '0';
				q_next		<= q_reg - 1;
				if q_next = 0 then
					state_next	<= zero;
				end if;
		end case;
	end process;
end fsmd_arch;

