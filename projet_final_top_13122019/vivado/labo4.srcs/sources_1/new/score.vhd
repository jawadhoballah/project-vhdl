----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/29/2019 07:30:18 PM
-- Design Name: 
-- Module Name: score - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity score is
    Port ( clk_2 :       in STD_LOGIC;
           reset :       in STD_LOGIC;
           collision :   in STD_LOGIC;
           start    :    in STD_LOGIC;
           score        :out  STD_LOGIC_vector( 9 downto 0);
           dj1        :out STD_LOGIC_vector( 3 downto 0);
           dj2        :out STD_LOGIC_vector( 3 downto 0);
           dj3        :out STD_LOGIC_vector( 3 downto 0)
           );
end score;

architecture Behavioral of score is
signal d1 : unsigned(3 downto 0);
signal d2 : unsigned(3 downto 0);
signal d3 : unsigned(3 downto 0);

signal s : Integer := 0; 

begin
    process (clk_2, reset, collision)
    begin
        if (collision='1' or reset='0') then
            s  <= 0;
            d1 <= (others=>'0');
            d2 <=  (others=>'0');
            d3<=  (others=>'0');
        elsif (rising_edge(clk_2))then
            if start = '1' then
                s  <= s +1 ;
         
        
                score  <= std_logic_vector(to_unsigned (s,10));
                
                if(d1 = 9) then
                    d1 <=  (others=>'0'); 
                    if(d2 = 9) then
                        d2 <=  (others=>'0');
                        if(d3 = 9) then
                            d3 <=  (others=>'0'); 
                        else 
                            d3 <= d3+1;
                        end if;
                    else 
                        d2 <= d2+1;
                    end if;
                else 
                    d1 <= d1+1;
                end if;
            end if;
        end if;
        
    end process;
 
  
  dj1 <=std_logic_vector(d1);
  dj2 <=std_logic_vector(d2);
  dj3 <=std_logic_vector(d3);




end Behavioral;
