----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/29/2019 06:49:10 PM
-- Design Name: 
-- Module Name: clock_dif - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity clock_dif is
    Port ( clk : in STD_LOGIC;
           clk_2 : out STD_LOGIC;
           score : in STD_LOGIC_VECTOR (9 downto 0);
           collision : in STD_LOGIC;
           reset : in STD_LOGIC);
end clock_dif;

architecture Behavioral of clock_dif is

signal clk_a  : std_logic:='0';
signal clk_b  : std_logic:='0';
signal clk_c  : std_logic:='0';
signal clk_z  : std_logic:='0';
signal z      : integer:=0;
signal a      : integer:=0;
signal b      : integer:=0;
signal c      : integer:=0;
begin

    process (clk,reset, collision, clk_z, clk_a, clk_b, clk_c)
    begin
        if (reset='0')or (collision='1') then
            clk_2<=clk_z;
        elsif (rising_edge(clk))then
            if(score<"0000010100") then
                clk_2<=clk_z;
            elsif("0000010100"<=score) and (score<"0000101000") then 
                clk_2<=clk_a;
            elsif("0000101000"<=score) and (score<"0000111100") then 
                clk_2<=clk_b;
            elsif("0000111100"<=score) and (score<"0001100100") then 
                clk_2<=clk_c;
            elsif("0001100100"<=score) then
                clk_2<=clk_c;
            end if;
        end if;
    end process;
   
    process(clk)
    begin

        if(rising_edge(clk))then

            z<=z+1;
            a<=a+1;
            b<=b+1;
            c<=c+1;

            --if(z=200) then
            if(z=100000000) then

                clk_z <= not clk_z;

                z<=0;
            end if;

            --if(a=100) then
            if(a=75000000) then

                clk_a <= not clk_a;
                a<=0;
            end if;

            --if(b=50) then
            if(b=50000000) then

                clk_b <= not clk_b;
                b<=0;
            end if;

            --if(c=30) then
            if(c=25000000) then

                clk_c <= not clk_c;
                c<=0;
            end if;


        end if;
    end process;
end Behavioral;
