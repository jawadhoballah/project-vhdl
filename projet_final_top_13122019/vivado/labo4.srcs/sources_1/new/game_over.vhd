----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/29/2019 04:15:39 PM
-- Design Name: 
-- Module Name: game_over - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity game_over is
    Port (
           position_voiture : in STD_LOGIC_VECTOR (15 downto 0);
           derniere_route : in STD_LOGIC_VECTOR (15 downto 0);
           collision : out std_logic);
end game_over;

architecture Behavioral of game_over is
 signal s: STD_LOGIC_VECTOR (15 downto 0) ;

begin
s  <= position_voiture and derniere_route ;
collision <= '0' when (s = "0000000000000000") else '1';




end Behavioral;
