----------------------------------------------------------------------------------
-- Fatameh Gholami
-- [ GHOF08549508]
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
USE IEEE.numeric_std.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_textio.all;
use std.textio.all;

entity voiture_tb is
end voiture_tb;

architecture tb1 of voiture_tb is 
		signal clk              :  std_logic := '0';
        signal   btn_g          :  std_logic := '0';
        signal btn_d            :  std_logic  :='0';
        signal  position_v      :  std_logic_vector(15 downto 0) := (others => '0'); 
        signal reset            :  std_logic := '1';
        signal collision        :  std_logic := '0';
       
begin  -- tb1

  DUT : entity work.voiture
    port map (
      clk => clk,
	 btn_g  => btn_g ,
	  btn_d => btn_d,
	  position_v   => position_v  ,
	  reset => reset,
	  collision   => collision,
    start       => '1'
	 );
  generateur : process
  begin
  
    wait for 5 ns;
	clk<= not clk;
	
  end process generateur;

process
begin

btn_g<='1';

 wait for 10 ns;
 btn_g<='0';
 
 wait for 16 ns;
 
 btn_g<='1';

 wait for 15 ns;
 btn_g<='0';
 
 wait for 16 ns;
 btn_g<='1';

 wait for 15 ns;
 btn_g<='0';
 
 wait for 16 ns;
 btn_g<='1';

 wait for 15 ns;
 btn_g<='0';
 
 wait for 16 ns;
 btn_g<='1';

 wait for 15 ns;
 btn_g<='0';
 
 wait for 16 ns;
 btn_g<='1';

 wait for 5 ns;
 btn_g<='0';
 
 wait for 6 ns;
 btn_g<='1';

 wait for 5 ns;
 btn_g<='0';
 
 wait for 6 ns;
 btn_g<='1';

 wait for 5 ns;
 btn_g<='0';
 
 wait for 6 ns;
 btn_g<='1';

 wait for 5 ns;
 btn_g<='0';
 
 wait for 6 ns;
 btn_g<='1';

 wait for 5 ns;
 btn_g<='0';
 
 wait for 6 ns;
 btn_g<='1';

 wait for 5 ns;
 btn_g<='0';
 
 wait for 6 ns;
 
 
 btn_d<='1';

 wait for 5 ns;
 btn_d<='0';
 
 wait for 6 ns;
 btn_d<='1';

 wait for 5 ns;
 btn_d<='0';
 
 wait for 6 ns;
 btn_d<='1';

 wait for 5 ns;
 btn_d<='0';
 
 wait for 6 ns;
 btn_d<='1';

 wait for 5 ns;
 btn_d<='0';
 
 wait for 6 ns;
 btn_d<='1';

 wait for 5 ns;
 btn_d<='0';
 
 wait for 6 ns;
 
 
 
 
 
 
 
 
 
 
 
 
 end process;
 
 end tb1;
 
 
 
 
 
 
 
 
 
 
 