----------------------------------------------------------------------------------
-- Jawad Hoballah
-- [ HOBJ28039504 ]
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
USE IEEE.numeric_std.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_textio.all;
use std.textio.all;

entity game_over_tb is
end game_over_tb;

architecture tb1 of game_over_tb is

  signal  position_voiture    :  std_logic_vector(15 downto 0) := (others => '0');
  signal  derniere_route      :  std_logic_vector(15 downto 0) := (others => '0');
  signal  collision           :  std_logic  :='0';

  begin  -- tb1

  DUT : entity work.game_over
  port map (
    position_voiture => position_voiture,
    derniere_route => derniere_route,
    collision => collision
  );

  generateur1 : process
  begin
    wait for 5 ns;

    position_voiture <= "0000000001000000";
    derniere_route <=   "1111111110011111";
    wait for 5 ns;
    position_voiture <= "0000000001000000";
    derniere_route <=   "1111111110111111";
    wait for 5 ns;
    position_voiture <= "0000000001000000";
    derniere_route <=   "1111111101011111";
    wait for 5 ns;
    position_voiture <= "0000000001000000";
    derniere_route <=   "1111111001111111";
  end process;
end tb1;