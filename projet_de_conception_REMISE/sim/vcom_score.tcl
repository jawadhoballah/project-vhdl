# repertoire contenant le code HDL a tester
set SRCDIR "../src"

# lier la librairie work au sous-repertoire work
vlib work
vmap work work

# code source pour le composant a tester
vcom ${SRCDIR}/score.vhd


# code source du testbench
vcom score_tb.vhd
