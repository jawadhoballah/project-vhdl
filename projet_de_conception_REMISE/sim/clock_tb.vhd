----------------------------------------------------------------------------------
-- Fatameh Gholami
-- [ GHOF08549508]
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
USE IEEE.numeric_std.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_textio.all;
use std.textio.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity clock_tb is
--  Port ( );
end clock_tb;

architecture tb1 of clock_tb is
signal clk                  :  std_logic := '0';
signal clk_2                  :  std_logic := '0';
signal   score          	:  std_logic_vector(9 downto 0) := (others => '0');
signal collision                 :  std_logic := '0';
signal reset                  :  std_logic := '1';
begin


 DUT : entity work.clock_dif
    port map (
      clk => clk,
	  clk_2=> clk_2,
	  score => score,
	  collision => collision,
      reset  => reset );
 process
  begin
  
    wait for 5 ns;
	clk<= not clk;
	
  end process;
  
  process
  begin
  wait for 5 ns;
  score<="0001100000";
  
  wait for 20 ns;
  
  score<="0001100111";
  
   wait for 20 ns;
  score<="0011001111";
  
  wait for 20 ns;
  
  
   wait for 20 ns;
  score<="0110010111";
  
  wait for 20 ns;
  
   wait for 20 ns;
  score<="1100100111";
  
  wait for 20 ns;
  
  collision<='1';
  
  wait for 10 ns;
  
   collision<='0';
  wait for 30 ns;
  
  end process;
end tb1;
