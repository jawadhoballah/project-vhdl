----------------------------------------------------------------------------------
-- Jawad Hoballah
-- [ HOBJ28039504 ]
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
USE IEEE.numeric_std.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_textio.all;
use std.textio.all;
   
entity score_tb is
	generic (	clk_period_gen		: integer := 10	-- clock period
	);
end score_tb;

architecture tb1 of score_tb is
	constant	CLK_PERIOD 		: time	:= (clk_period_gen) * 1 ns;

	signal clk_2 		: std_logic  :='0';
	signal reset 		: std_logic  :='1';
	signal collision   	: std_logic  :='0';
	signal start 		: std_logic	 :='0';
	signal score       	: std_logic_vector(9 downto 0) := (others => '0');
	signal dj1	     	: std_logic_vector(3 downto 0) := (others => '0');
	signal dj2         	: std_logic_vector(3 downto 0) := (others => '0');
	signal dj3         	: std_logic_vector(3 downto 0) := (others => '0');

begin  -- tb1

	DUT : entity work.score
		port map (
			clk_2		=> clk_2,
			reset 		=> reset,
			collision 	=> collision,
			start 		=> '1',
			score 		=> score,
			dj1 		=> dj1,
			dj2 		=> dj2,
			dj3 		=> dj3
		);

	clk_2 <= not clk_2 after CLK_PERIOD/2;

	-- First reset all components
	reset 	<= '0' after 2 ns, '1' after 4 ns;

	-- simulate collision
	collision 	<= '1' after 150 ns, '0' after 155 ns;
end tb1;