----------------------------------------------------------------------------------
-- Pierre Tremblay-Thériault
-- [ TREP17068309 ]
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity route is
	port(
		clk, reset			: in	std_logic;
		collision 			: in 	std_logic;
		start               : in    std_logic;
		matrice_route_l4 	: out	std_logic_vector(15 downto 0);
		matrice_route_l3 	: out	std_logic_vector(15 downto 0);
		matrice_route_l2 	: out	std_logic_vector(15 downto 0);
		matrice_route_l1 	: out	std_logic_vector(15 downto 0)
	);
end route;

architecture fsmd_arch of route is
	type state_type is (init, switch);
	signal state_reg, state_next 					: state_type;

	signal routes_l4_reg, routes_l4_next			: std_logic_vector(15 downto 0) := (others => '0');
	signal routes_l3_reg, routes_l3_next			: std_logic_vector(15 downto 0) := (others => '0');
	signal routes_l2_reg, routes_l2_next			: std_logic_vector(15 downto 0) := (others => '0');
	signal routes_l1_reg, routes_l1_next			: std_logic_vector(15 downto 0) := (others => '0');
	signal rnd 										: std_logic := '0';

	-- random (ring oscillator) SOURCE: https://stackoverflow.com/questions/50897986/how-to-create-a-xored-ring-oscillator-using-vhdl
	signal osc_chain							: std_logic_vector(2 downto 0) := (others => '0');
begin
	-- random (ring oscillator)
	gen_inv: for i in 0 to 2 generate
		osc_chain(i) <= not osc_chain((i+1) mod 3) after (10 + i) * 1 ns;
	end generate;

	-- FSMD state & data registers
	process( clk, reset )
	begin
		if reset = '1' then
			state_reg 			<= init;
		elsif rising_edge(clk) then
			state_reg 			<= state_next;
			routes_l4_reg		<= routes_l4_next;
			routes_l3_reg		<= routes_l3_next;
			routes_l2_reg		<= routes_l2_next;
			routes_l1_reg		<= routes_l1_next;
			rnd 				<= osc_chain(0);
		end if;
	end process;

	matrice_route_l4 <= routes_l4_reg;
	matrice_route_l3 <= routes_l3_reg;
	matrice_route_l2 <= routes_l2_reg;
	matrice_route_l1 <= routes_l1_reg;

	-- next-state logic & data path functional units
	process( state_reg, routes_l1_reg, routes_l2_reg, routes_l3_reg, routes_l4_reg, collision, rnd, start )
	
	begin
		state_next 			<= state_reg;
		routes_l4_next 		<= routes_l4_reg;
		routes_l3_next 		<= routes_l3_reg;
		routes_l2_next 		<= routes_l2_reg;
		routes_l1_next 		<= routes_l1_reg;

		case state_reg is
			when init =>
				routes_l1_next 	<= "1111111001111111";
				routes_l2_next 	<= "1111111001111111";
				routes_l3_next 	<= "1111111001111111";
				routes_l4_next 	<= "1111111001111111";
				if start = '1' then
				    state_next 				<= switch;
				end if;
			when switch =>
				if collision = '1' then
					state_next <= init;
				else
					if rnd = '1' then 	-- rotation droite
						if routes_l4_reg(1 downto 0) /= "00" then
							routes_l4_next <= '1' & routes_l4_reg(15 downto 1);
						end if;
					else				-- rotation gauche
						if routes_l4_reg(15 downto 14) /= "00"  then
							routes_l4_next <= routes_l4_reg(14 downto 0) & '1';
						end if;
					end if;
					routes_l3_next <= routes_l4_reg;
					routes_l2_next <= routes_l3_reg;
					routes_l1_next <= routes_l2_reg;
				end if;
		end case;
	end process;
end fsmd_arch;