----------------------------------------------------------------------------------
-- Fatameh Gholami
-- [ GHOF08549508 ]
--
-- Pierre Tremblay-Thériault
-- [ TREP17068309 ]
--
-- Jawad Hoballah
-- [ HOBJ28039504 ]
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top is
    generic (
        db_clock_repeat_gen : integer := 22
    );
    port (
        clk         : in    std_logic;
        rstn        : in    std_logic;
        btnl        : in    std_logic;
        btnr        : in    std_logic;
        btnc        : in    std_logic;
        oled_sdin   : out   std_logic;
        oled_sclk   : out   std_logic;
        oled_dc     : out   std_logic;
        oled_res    : out   std_logic;
        oled_vbat   : out   std_logic;
        oled_vdd    : out   std_logic
    );  
end top;


architecture behavioral of top is

    component debounce is
    generic (
        db_clock_repeat : integer := 22
    );
    port (
        clk, reset      : in    std_logic;
        sw              : in    std_logic;
        db_level        : out   std_logic
    );
    end component;

    component rising_edge_detect is
    port (
        clk, rstn       : in    std_logic;
        input           : in    std_logic;
        pulse           : out   std_logic
    );
    end component;
    
    component voiture is
        port ( 
            clk         : in STD_LOGIC;
            btn_g       : in STD_LOGIC;
            btn_d       : in STD_LOGIC;
            position_v  : out STD_LOGIC_VECTOR (15 downto 0);
            reset       : in STD_LOGIC;
            collision   : in STD_LOGIC;
            start       : in STD_LOGIC
        );
    end component;
    
    component route is
	port(
		clk, reset			: in	std_logic;
		collision 			: in 	std_logic;
		start               : in    std_logic;
		matrice_route_l4 	: out	std_logic_vector(15 downto 0);
		matrice_route_l3 	: out	std_logic_vector(15 downto 0);
		matrice_route_l2 	: out	std_logic_vector(15 downto 0);
		matrice_route_l1 	: out	std_logic_vector(15 downto 0)
	);
    end component;
    
    component game_over is
    port (
           position_voiture : in STD_LOGIC_VECTOR (15 downto 0);
           derniere_route : in STD_LOGIC_VECTOR (15 downto 0);
           collision : out std_logic);
    end component;
    
    component clock_dif is
    port ( clk : in STD_LOGIC;
           clk_2 : out STD_LOGIC;
           score : in STD_LOGIC_VECTOR (9 downto 0);
           collision : in STD_LOGIC;
           reset : in STD_LOGIC);
    end component;
    
    component score is
    port ( clk_2 :       in STD_LOGIC;
           reset :       in STD_LOGIC;
           collision :   in STD_LOGIC;
           start     :   in STD_LOGIC;
           score        :out  STD_LOGIC_vector( 9 downto 0);
           dj1        :out STD_LOGIC_vector( 3 downto 0);
           dj2        :out STD_LOGIC_vector( 3 downto 0);
           dj3        :out STD_LOGIC_vector( 3 downto 0)
           );
    end component;

    component OLEDCtrl is
    port(
        clk                 : in    std_logic;

        write_start         : in    std_logic;      
        write_ascii_data    : in    std_logic_vector(7 downto 0);
        write_base_addr     : in    std_logic_vector(8 downto 0);
        
        update_start        : in    std_logic;
        update_clear        : in    std_logic;

        disp_on_start       : in    std_logic;
        disp_off_start      : in    std_logic;
        toggle_disp_start   : in    std_logic;
        
        write_ready         : out   std_logic;
        update_ready        : out   std_logic;
        disp_on_ready       : out   std_logic;
        disp_off_ready      : out   std_logic;
        toggle_disp_ready   : out   std_logic;

        SDIN                : out   std_logic;
        SCLK                : out   std_logic;
        DC                  : out   std_logic;
        RES                 : out   std_logic;
        VBAT                : out   std_logic;
        VDD                 : out   std_logic
    );
    end component;
    
    signal write_start_sig, write_start_sig_next            : std_logic := '0';
    signal write_ascii_data_sig, write_ascii_data_sig_next  : std_logic_vector(7 downto 0) := (others => '0');
    signal write_base_addr_sig, write_base_addr_sig_next    : std_logic_vector(8 downto 0) := (others => '0');
    signal update_start_sig, update_start_sig_next          : std_logic := '0';
    signal update_clear_sig, update_clear_sig_next          : std_logic := '0';
    signal disp_on_start_sig, disp_on_start_sig_next        : std_logic := '0';

    signal write_ready_sig                                  : std_logic := '0';
    signal update_ready_sig                                 : std_logic := '0';
    signal disp_on_ready_sig                                : std_logic := '0';

    signal init_ready                                       : std_logic := '0';
    signal process_ready                                    : std_logic := '0';

    signal reset_high                                       : std_logic;
    signal reset_low                                        : std_logic;

    signal db_tick_btnr                                     : std_logic;
    signal db_tick_btnl                                     : std_logic;
    signal db_tick_btnc                                     : std_logic;
    signal db_level_btnr                                    : std_logic;
    signal db_level_btnl                                    : std_logic;
    signal db_level_btnc                                    : std_logic;

    type state_type is (init, clear_request, clear, update_line_1, update_line_2, update_line_3, update_line_4, display_request, display);
    signal state_reg, state_next    : state_type;

    signal mat_route_4, mat_route_3, mat_route_2, mat_route_1   : std_logic_vector(15 downto 0);
    signal sig_collision                                        : std_logic := '0';
    signal pos_v                                                : std_logic_vector(15 downto 0);
    signal sig_clk_2                                            : std_logic := '0';
    signal sig_score                                            : std_logic_vector(9 downto 0);
    signal digit3, digit2, digit1                               : std_logic_vector(3 downto 0);
    signal sig_start                                            : std_logic := '0';
    signal line_number_reg, line_number_next                    : unsigned(1 downto 0) := "00";
    signal char_pos_reg, char_pos_next                          : unsigned(3 downto 0) := "0000";
    signal initialized, initialized_next                        : std_logic := '0';
begin

    -- BTNR
    inst_debounce_btn_BTNR: debounce
    generic map (
        db_clock_repeat     => db_clock_repeat_gen
    )
    port map (
        clk         => clk,
        reset       => reset_high,
        sw          => btnr,
        db_level    => db_level_btnr
    );

    inst_rising_edge_detect_BTNR: rising_edge_detect
    port map (
        clk         => clk,
        rstn        => reset_low,
        input       => db_level_btnr,
        pulse       => db_tick_btnr
    );

    -- BTNL
    inst_debounce_btn_BTNL: debounce
    generic map (
        db_clock_repeat     => db_clock_repeat_gen
    )
    port map (
        clk         => clk,
        reset       => reset_high,
        sw          => btnl,
        db_level    => db_level_btnl
    );

    inst_rising_edge_detect_BTNL: rising_edge_detect
    port map (
        clk         => clk,
        rstn        => reset_low,
        input       => db_level_btnl,
        pulse       => db_tick_btnl
    );
    
    -- BTNC
    inst_debounce_btn_BTNC: debounce
    generic map (
        db_clock_repeat     => db_clock_repeat_gen
    )
    port map (
        clk         => clk,
        reset       => reset_high,
        sw          => btnc,
        db_level    => db_level_btnc
    );

    inst_rising_edge_detect_BTNC: rising_edge_detect
    port map (
        clk         => clk,
        rstn        => reset_low,
        input       => db_level_btnc,
        pulse       => db_tick_btnc
    );
    
    inst_voiture: voiture
        port map( 
            clk         => clk,
            btn_g       => db_tick_btnr,
            btn_d       => db_tick_btnl,
            position_v  => pos_v,
            reset       => reset_low,
            collision   => sig_collision,
            start       => sig_start
        );
        
    inst_route: route
    port map(
		clk                 => sig_clk_2,
		reset			    => reset_high,
        collision           => sig_collision,
        start               => sig_start,
		matrice_route_l4 	=> mat_route_4,
		matrice_route_l3 	=> mat_route_3,
		matrice_route_l2 	=> mat_route_2,
		matrice_route_l1 	=> mat_route_1
	);
	
	inst_gameover: game_over
    port map (
           position_voiture     => pos_v,
           derniere_route       => mat_route_1,
           collision            => sig_collision
     );
     
    inst_clock: clock_dif
    port map(
           clk          => clk,
           clk_2        => sig_clk_2,
           score        => sig_score,
           collision    => sig_collision,
           reset        => reset_low
    );
    
    inst_score: score
    port map( clk_2     => sig_clk_2,
           reset        => reset_low,
           collision    => sig_collision,
           start        => sig_start,
           score        => sig_score,
           dj1          => digit1,
           dj2          => digit2,
           dj3          => digit3
    );
     
    inst_controleur: OLEDCtrl
    port map(
            clk                 => clk,
            write_start         => write_start_sig,     
            write_ascii_data    => write_ascii_data_sig,
            write_base_addr     => write_base_addr_sig,

            update_start        => update_start_sig,
            update_clear        => update_clear_sig,

            disp_on_start       => disp_on_start_sig,
            disp_off_start      => '0',
            toggle_disp_start   => '0',

            write_ready         => write_ready_sig,
            update_ready        => update_ready_sig,
            disp_on_ready       => disp_on_ready_sig,
            disp_off_ready      => open,
            toggle_disp_ready   => open,

            SDIN                => oled_sdin,
            SCLK                => oled_sclk,
            DC                  => oled_dc,
            RES                 => oled_res,
            VBAT                => oled_vbat,
            VDD                 => oled_vdd
    );

    -- Attribution du bouton reset
    reset_high <= not rstn;
    reset_low <= rstn;
    
    -- reset_high <= db_tick_btnd;
    -- reset_low <= not db_tick_btnd;

    init_ready <= disp_on_ready_sig;
    process_ready <= update_ready_sig or write_ready_sig;

    process( clk, init_ready, process_ready, reset_high, db_tick_btnc, sig_collision )
    begin
        if reset_high = '1' or sig_collision = '1' then
            state_reg               <= init;
            write_start_sig         <= '0';
            update_start_sig        <= '0';
            update_clear_sig        <= '0';
            disp_on_start_sig       <= '0';
            sig_start               <= '0';
            char_pos_reg            <= (others=>'0');
            line_number_reg         <= (others=>'0');
        elsif rising_edge(CLK) then
            if db_tick_btnc = '1' then
                sig_start   <= '1';
            end if;
        
            state_reg               <= state_next;
            disp_on_start_sig       <= disp_on_start_sig_next;
            update_clear_sig        <= update_clear_sig_next;
            update_start_sig        <= update_start_sig_next;
            write_ascii_data_sig    <= write_ascii_data_sig_next;
            write_base_addr_sig     <= write_base_addr_sig_next;
            write_start_sig         <= write_start_sig_next;
            char_pos_reg            <= char_pos_next;
            line_number_reg         <= line_number_next;
            initialized             <= initialized_next;
        end if;
    end process;

    process( state_reg, initialized, init_ready, process_ready, disp_on_start_sig, update_clear_sig, write_start_sig, update_start_sig, write_ascii_data_sig, write_base_addr_sig, char_pos_reg, line_number_reg, pos_v, mat_route_1, mat_route_2, mat_route_3, mat_route_4, digit3, digit2, digit1 )
    begin
        state_next                  <= state_reg;
        disp_on_start_sig_next      <= disp_on_start_sig;
        update_clear_sig_next       <= update_clear_sig;
        update_start_sig_next       <= update_start_sig;
        write_ascii_data_sig_next   <= write_ascii_data_sig;
        write_base_addr_sig_next    <= write_base_addr_sig;
        write_start_sig_next        <= write_start_sig;
        char_pos_next               <= char_pos_reg;
        line_number_next            <= line_number_reg;
        initialized_next            <= initialized;

        case state_reg is
            when init =>
                if initialized = '0' then
                    if init_ready = '1' then
                        disp_on_start_sig_next <= '1';
                        initialized_next       <= '1';
                        state_next             <= clear_request;
                    end if;
                else
                    state_next             <= clear_request;
                end if;
            when clear_request =>
                disp_on_start_sig_next      <= '0';
                if process_ready = '1' then
                    update_start_sig_next   <= '1';
                    update_clear_sig_next   <= '1';
                    state_next              <= clear;
                end if;
            when clear =>
                update_start_sig_next       <= '0';
                update_clear_sig_next       <= '0';
                if process_ready = '1' then
                    state_next  <= update_line_4;
                end if;
            when update_line_1 =>
                if process_ready = '1' then
                    -- mise a jour de la memoire
                    if pos_v(to_integer(char_pos_reg)) = '1' then
                        write_ascii_data_sig_next   <= x"40";
                    elsif mat_route_1(to_integer(char_pos_reg)) = '1' then
                        write_ascii_data_sig_next   <= x"23";
                    else
                        write_ascii_data_sig_next   <= x"20";
                    end if;
                    write_base_addr_sig_next    <= std_logic_vector(line_number_reg) & std_logic_vector(char_pos_reg) & "000";
                    write_start_sig_next        <= '1';
                    state_next                  <= display_request;
                end if;
            when update_line_2 =>
                if process_ready = '1' then
                    -- mise a jour de la memoire
                    if mat_route_2(to_integer(char_pos_reg)) = '1' then
                        write_ascii_data_sig_next   <= x"23";
                    else
                        write_ascii_data_sig_next   <= x"20";
                    end if;
                    write_base_addr_sig_next    <= std_logic_vector(line_number_reg) & std_logic_vector(char_pos_reg) & "000";
                    write_start_sig_next        <= '1';
                    state_next                  <= display_request;
                end if;
            when update_line_3 =>
                if process_ready = '1' then
                    -- mise a jour de la memoire
                    if mat_route_3(to_integer(char_pos_reg)) = '1' then
                        write_ascii_data_sig_next   <= x"23";
                    else
                        write_ascii_data_sig_next   <= x"20";
                    end if;
                    write_base_addr_sig_next    <= std_logic_vector(line_number_reg) & std_logic_vector(char_pos_reg) & "000";
                    write_start_sig_next        <= '1';
                    state_next                  <= display_request;
                end if;
            when update_line_4 =>
                if process_ready = '1' then
                    -- mise a jour de la memoire
                    if char_pos_reg < 3 then
                        if char_pos_reg = 0 then
                            write_ascii_data_sig_next   <= "0011" & digit3;
                        elsif char_pos_reg = 1 then
                            write_ascii_data_sig_next   <= "0011" & digit2;
                        else
                            write_ascii_data_sig_next   <= "0011" & digit1;
                        end if;
                    else
                        if mat_route_4(to_integer(char_pos_reg)) = '1' then
                            write_ascii_data_sig_next   <= x"23";
                        else
                            write_ascii_data_sig_next   <= x"20";
                        end if;
                    end if;
                    
                    write_base_addr_sig_next    <= std_logic_vector(line_number_reg) & std_logic_vector(char_pos_reg) & "000";
                    write_start_sig_next        <= '1';
                    state_next                  <= display_request;
                end if;
            when display_request =>
                write_start_sig_next        <= '0';
               
                if process_ready = '1' then
                    if char_pos_reg = 15 then
                        char_pos_next <= (others=>'0');
                        if line_number_reg = 3 then
                            update_start_sig_next   <= '1';
                            state_next      <= display;
                        else
                            line_number_next    <= line_number_reg + 1;
                            if line_number_reg = 0 then
                                state_next      <= update_line_3;
                            elsif line_number_reg = 1 then
                                state_next      <= update_line_2;
                            elsif line_number_reg = 2 then
                                state_next      <= update_line_1;
                            elsif line_number_reg = 3 then
                                state_next      <= update_line_4;
                            end if;
                        end if;
                    else
                        char_pos_next               <= char_pos_reg + 1;
                        if line_number_reg = 0 then
                            state_next              <= update_line_4;
                        elsif line_number_reg = 1 then
                            state_next              <= update_line_3;
                        elsif line_number_reg = 2 then
                            state_next              <= update_line_2;
                        elsif line_number_reg = 3 then
                            state_next              <= update_line_1;
                        end if;
                    end if;
                end if;
            when display =>
                if process_ready = '1' then
                    update_start_sig_next       <= '0';
                    line_number_next <= (others=>'0');
                    state_next                  <= update_line_4;
                end if;
        end case;
    end process;

end behavioral;