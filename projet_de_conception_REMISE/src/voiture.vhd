----------------------------------------------------------------------------------
-- Fatameh Gholami
-- [ GHOF08549508]
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.all;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity voiture is
    Port ( clk : in STD_LOGIC;
           btn_g : in STD_LOGIC;
           btn_d : in STD_LOGIC;
           position_v : out STD_LOGIC_VECTOR (15 downto 0);
           reset : in STD_LOGIC;
           collision : in STD_LOGIC;
           start        : in STD_LOGIC);
end voiture;

architecture Behavioral of voiture is
signal position_v_w : STD_LOGIC_VECTOR (15 downto 0):= "0000000010000000";

begin

    process (clk,reset,collision)
    begin
        if (reset='0') or (collision='1')then
            position_v_w<="0000000010000000";
        elsif (rising_edge(clk))then
            if start = '1' then
                if(btn_g='1') then
                    if(position_v_w="1000000000000000") then
                        position_v_w<="1000000000000000";
                    else
                        position_v_w<=position_v_w(14 downto 0)& '0';
                    end if;
                end if;
                
                if(btn_d='1') then
                    if(position_v_w="0000000000000001") then
                        position_v_w<="0000000000000001";
                    else
                        position_v_w<= '0' & position_v_w(15 downto 1);
                    end if;
                end if;
           end if;
        end if;
    end process;
    
    position_v<= position_v_w;

end Behavioral;
