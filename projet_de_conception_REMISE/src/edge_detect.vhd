library ieee;
use ieee.std_logic_1164.all;

entity rising_edge_detect is
    port ( clk   : in  	std_logic;
           rstn  : in  	std_logic;
           input : in  	std_logic;
           pulse : out  std_logic
    );
end rising_edge_detect;

architecture Behavioral of rising_edge_detect is
	signal reg_in	: std_logic;
	signal reg_in_d	: std_logic;
begin
	rising_edge_detector : process(clk,rstn)
	begin
	  if(rstn='0') then
		 reg_in	  <= '0';
		 reg_in_d <= '0';
	  elsif(rising_edge(clk)) then
		 reg_in   <= input;
		 reg_in_d <= reg_in;
	  end if;
	end process rising_edge_detector;
	
	pulse <= not reg_in_d and reg_in;

end Behavioral;